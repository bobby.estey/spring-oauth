# Spring Boot - OAUTH

## References / Links

- [OAUTH Login Recording](./docs/oauthLogin.mp4)
- [Spring OAUTH Recording - Tutorial](https://www.youtube.com/watch?v=us0VjFiHogo)
- [gitlab - application.properties](https://stackoverflow.com/questions/49332008/spring-security-5-oauth2-client-with-gitlab-failed)

## Initialization

- [Spring Initializr](https://start.spring.io)

![Spring Initializr Configuration](./docs/springInitializr.png)

## Project Configuration

|Key|Value|
|--|--|
|Project|Maven|
|Language|Java|
|Package Name|us.cv64.spring-oauth|
|Packaging|Jar|
|Java|20|
|Dependencies|- Spring Web<br>- OAuth2 Client|

- Press Generate - build the project

## Integrated Development Environment (IDE)

## Gitlab Setup

- Navigate to the Applications Link
     - Right Click User Profile
     - Select Applications

![Applications](./docs/applications.png)

- Create Application
     - Name:  Spring OAUTH
     - Callback URL:  http://localhost:port/login/oauth2/code/gitlab
     - Confidential:  Yes
     - Scopes:  read_user

![OAUTH New Application](./docs/oauthNewApplication.png)

![OAUTH Parameters](./docs/oauthParameters.png)

- List OAUTH Applications

![OAUTH List](./docs/oauthList.png)

## OAUTH Execution

#### Unsecure Login - http://localhost:6464

![Unsecure Login](./docs/unsecureLogin.png)

#### Secure Login - http://localhost:6464/login

-  Press GitLab

![Press GitLab](./docs/pressGitLab.png)

- Press Site Connection Secure

![Site Connection Secure](./docs/siteConnectionSecure.png)

- Enter Username / Password

![Login Credentials](./docs/loginCredentials.png)

- Redirection Link

![Redirection](./docs/redirection.png)

- Secure Login

![Secure Login](./docs/secureLogin.png)
